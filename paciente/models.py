from django.db import models

# Create your models here.
class Paciente(models.Model):
	DOCUMENT_CHOICES = (('DNI','DNI'),
					('PASAPORTE','Pasaporte'),
					('CARNET','Carnet'),
					('RUC','Ruc'),
					)
	tipo_documento = models.CharField(
		max_length=10,
		choices = DOCUMENT_CHOICES,
		default='DNI',
		)
	numero_documento = models.CharField(max_length=50)
	nombres = models.CharField(max_length=50)
	apellido_paterno = models.CharField(max_length=50)
	apellido_materno = models.CharField(max_length=50)
	GENERO_CHOICES = (
    		('MASCULINO','Masculino'),
    		('FEMENINO','Femenino')
    	)
	genero = models.CharField(max_length=10, choices = GENERO_CHOICES)
	fecha_nacimiento = models.DateField()
	regalo_preferido = models.CharField(max_length=30)
	edad = models.IntegerField()

	def __str__(self):
	    return '{} {} {}'.format(self.nombres, self.apellido_paterno, self.apellido_materno)
