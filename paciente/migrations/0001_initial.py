
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Paciente',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tipo_documento', models.CharField(choices=[('DNI', 'DNI'), ('PASAPORTE', 'Pasaporte'), ('CARNET', 'Carnet'), ('RUC', 'Ruc')], default='DNI', max_length=10)),
                ('numero_documento', models.CharField(max_length=50)),
                ('nombres', models.CharField(max_length=50)),
                ('apellido_paterno', models.CharField(max_length=50)),
                ('apellido_materno', models.CharField(max_length=50)),
                ('genero', models.CharField(choices=[('MASCULINO', 'Masculino'), ('FEMENINO', 'Femenino')], max_length=10)),
                ('fecha_nacimiento', models.DateField()),
                ('regalo_preferido', models.CharField(max_length=30)),
                ('edad', models.IntegerField()),
            ],
        ),
    ]
