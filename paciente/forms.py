from django import forms
from paciente.models import Paciente

class PacienteForm(forms.ModelForm):
	class Meta:
		model = Paciente

		fields = [
			'tipo_documento',
			'numero_documento',
			'nombres',
			'apellido_paterno',
			'apellido_materno',
			'genero',
			'fecha_nacimiento',
			'regalo_preferido',
			'edad',
		]

		labels = {
			'tipo_documento' : 'Tipo de Documento',
			'numero_documento' : 'Numero de Documento',
			'nombres' : 'Nombres',
			'apellido_paterno' : 'Apellido Paterno',
			'apellido_materno' : 'Apellido Materno' ,
			'genero' : 'Genero',
			'fecha_nacimiento' : 'Fecha de Nacimiento',
			'regalo_preferido' : 'Regalo Preferido',
			'edad':'Edad',
		}

		widgets = {
			'tipo_documento' : forms.Select(attrs = {'class' : 'form-control'}),
			'numero_documento' : forms.TextInput(attrs = {'class' : 'form-control'}),
			'nombres' : forms.TextInput(attrs = {'class' : 'form-control'}),
			'apellido_paterno' : forms.TextInput(attrs = {'class' : 'form-control'}),
			'apellido_materno' : forms.TextInput(attrs = {'class' : 'form-control','id':'apellido_materno'}),
			'genero' : forms.Select(attrs = {'class' : 'form-control','id':'combo_genero'}),
			'fecha_nacimiento' : forms.TextInput(attrs = {'type':'date','class' : 'form-control', 'id':'fecha_nacimiento'}),
			'regalo_preferido': forms.Select(attrs = {'class' : 'form-control', 'id':'combo_regalo','readonly':'readonly'}),
			'edad': forms.TextInput(attrs = {'class':'form-control', 'id':'edad','readonly':'readonly'}),
		}