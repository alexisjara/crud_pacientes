from django.shortcuts import render
from django.core.urlresolvers import reverse_lazy
from paciente.models import Paciente
from paciente.forms import PacienteForm
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

# Create your views here.
class PacienteList(ListView):
	model = Paciente
	template_name = 'persona_list.html'
	ordering = 'id'

class PacienteCreate(CreateView):
	model = Paciente
	form_class = PacienteForm
	template_name = 'paciente/paciente_form.html'
	success_url	= reverse_lazy('paciente:paciente_listar')

class PacienteUpdate(UpdateView):
	model = Paciente
	form_class = PacienteForm
	template_name = 'paciente/paciente_form.html'
	success_url	= reverse_lazy('paciente:paciente_listar')

class PacienteDelete(DeleteView):
	model = Paciente
	template_name = 'paciente/paciente_delete.html'
	success_url = reverse_lazy('paciente:paciente_listar')
