from django.conf.urls import url
from paciente.views import PacienteList, PacienteCreate, PacienteUpdate, PacienteDelete

urlpatterns = [
	url(r'^$', PacienteList.as_view(), name='paciente_listar'),
    url(r'^listar$', PacienteList.as_view(), name='paciente_listar'),
    url(r'^nuevo$', PacienteCreate.as_view(), name='paciente_crear'),
    url(r'^editar/(?P<pk>\d+)$', PacienteUpdate.as_view(), name='paciente_editar'),
    url(r'^eliminar/(?P<pk>\d+)$', PacienteDelete.as_view(), name='paciente_eliminar'),
]
