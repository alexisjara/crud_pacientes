//ASIGNANDO ATRIBUTO A UN INPUT
     var f_n = document.querySelector('#fecha_nacimiento');
      f_n.setAttribute("onchange", "myFunction()");


//FUNCION PARA CALCULAR EDAD DE ACUERDO AL ANIO Y MES
      function myFunction() {
        //CAPTURANDO FECHAS ACTUALES
        var fecha = new Date();
        var aniohoy = fecha.getFullYear();
        var meshoy = fecha.getMonth()+1;

        var fecha=document.getElementById("fecha_nacimiento").value;
        var values=fecha.split("-");

            var dia = values[2];
            var mes = values[1];
            var ano = values[0];

            var edad = aniohoy-ano;

            if(meshoy<mes){
              edad=edad-1;
            }
            else if(meshoy>=mes){
              edad=edad;
            }
            if(edad<0){
              edad=0;
            }
            $("#edad").val(edad);
      }

//FUNCION PARA VALIDAR EL INGRESO DE SOLO TEXTO CON JQUERY VALIDATE ADITIONAL METHODS
jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-z\s]+$/i.test(value);
}, "Solo letras");

//REGLAS PARA EL FORMULARIO USANDO JQUERY VALIDATE
$('#my_form').validate({
  rules : {
    numero_documento : {'required':true, number:true},
    nombres : {'required':true, number:false, lettersonly:true},
    apellido_paterno : {'required':true, lettersonly:true},
    apellido_materno : {'required':true, lettersonly:true},
    genero : {'required' : 'Campo requerido'},
    fecha_nacimiento : {'required' : 'Campo requerido'},
    regalo_preferido : {'required' : 'Campo requerido'},
    edad : {'required' : 'Campo requerido'}
  },

  messages : {
    numero_documento : {'required':'Campo requerido',number:'Solo Numero'},
    nombres : {'required': 'Campo requerido', number:'Solo Texto'},
    apellido_paterno : {'required': 'Campo requerido'},
    apellido_materno : {'required': 'Campo requerido'},
    genero : {'required' : 'Campo requerido'},
    fecha_nacimiento : {'required' : 'Campo requerido'},
    regalo_preferido : {'required' : 'Campo requerido'},
    edad : {'required' : 'Campo requerido'}
  }
});

//FUNCION PARA CREAR COMBOS DEPENDIENTES
var options = {
    MASCULINO : ["Pantalon","Camisa","Corbata"],
    FEMENINO : ["zapatos","Tacos","Vestidos"]
}

$(function(){
  var fillSecondary = function(){
    var selected = $('#combo_genero').val();
    $('#combo_regalo').empty();
    options[selected].forEach(function(element,index){
      $('#combo_regalo').append('<option value="'+element+'">'+element+'</option>');
    });
  }
  $('#combo_genero').change(fillSecondary);
  fillSecondary();
});
